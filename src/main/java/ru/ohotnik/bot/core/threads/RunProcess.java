package ru.ohotnik.bot.core.threads;

import ru.ohotnik.bot.core.action.ActionProcessor;
import ru.ohotnik.bot.core.action.IAction;

import java.security.SecureRandom;
import java.util.Date;

import static java.lang.System.currentTimeMillis;

/**
 * Created by ohotNik
 * Date : 26.10.14
 * Time : 14:39
 * Description :
 */
@SuppressWarnings("ClassExplicitlyExtendsThread")
public class RunProcess extends Thread {

  public static final int INT_DELTA = 3000;
  @SuppressWarnings({
      "StaticVariableMayNotBeInitialized", "StaticVariableNamingConvention"
      , "StaticNonFinalField"})
  private static RunProcess process;
  @SuppressWarnings("StaticNonFinalField")
  private static volatile boolean s_flag = true;

  public static RunProcess getProcess() {
    //noinspection StaticVariableUsedBeforeInitialization
    if (process == null) {
      //noinspection NonThreadSafeLazyInitialization
      process = new RunProcess();
    }
    return process;
  }

  @SuppressWarnings("UnusedDeclaration")
  public static void stopProcess() {
    s_flag = false;
  }

  /**
   * If this thread was constructed using a separate
   * <code>Runnable</code> run object, then that
   * <code>Runnable</code> object's <code>run</code> method is called;
   * otherwise, this method does nothing and returns.
   * <p/>
   * Subclasses of <code>Thread</code> should override this method.
   *
   * @see #start()
   * @see #stop()
   */
  @SuppressWarnings({"RefusedBequest", "UseOfSystemOutOrSystemErr"})
  @Override
  public void run() {

    long now = currentTimeMillis();
    System.out.println("Current time - " + new Date(now));
    ActionProcessor processor = ActionProcessor.getInstance();

    System.out.println("Starting new queue process...");
    try {
      //noinspection InfiniteLoopStatement
      while (s_flag) {
        IAction action = processor.peek();
        if (action == null) {
          //noinspection BusyWait,MagicNumber
          sleep((long) new SecureRandom().nextInt(INT_DELTA));
          continue;
        }
        long time = action.getTime();
        long delta = time - now;
        if (delta > 0L) {
          //noinspection BusyWait
          sleep(delta + (long) new SecureRandom().nextInt(INT_DELTA));
        }
        now = currentTimeMillis();
        if (action.getTime() != processor.peek().getTime() || now < action.getTime()) {
          continue;
        }
        action.execute();
        processor.poll();
      }
    } catch (InterruptedException e) {
      //noinspection CallToPrintStackTrace
      e.printStackTrace();
    }
  }

}
