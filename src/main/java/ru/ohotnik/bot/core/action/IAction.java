package ru.ohotnik.bot.core.action;

/**
 * Created by ohotNik
 * Date : 18.10.14
 * Time : 14:20
 * Description :
 */
public interface IAction extends Comparable<Object> {

  void execute();

  void update();

  long getTime();

}
