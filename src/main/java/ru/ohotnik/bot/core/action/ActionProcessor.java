package ru.ohotnik.bot.core.action;

import com.google.common.collect.Queues;
import ru.ohotnik.bot.core.threads.RunProcess;

import java.util.concurrent.PriorityBlockingQueue;

import static ru.ohotnik.bot.core.threads.RunProcess.getProcess;

/**
 * Created by ohotNik
 * Date : 18.10.14
 * Time : 14:25
 * Description :
 */
public class ActionProcessor {

  private final PriorityBlockingQueue<IAction> queue = Queues.newPriorityBlockingQueue();

  @SuppressWarnings({
      "StaticVariableMayNotBeInitialized", "StaticVariableNamingConvention"
      , "StaticNonFinalField"})
  private static ActionProcessor instance;

  @SuppressWarnings("SynchronizedMethod")
  public static synchronized ActionProcessor getInstance() {
    //noinspection StaticVariableUsedBeforeInitialization
    if (instance == null) {
      instance = new ActionProcessor();
    }
    return instance;
  }

  private ActionProcessor() {
    RunProcess process = getProcess();
    //noinspection CallToThreadStartDuringObjectConstruction
    process.start();
  }

  public IAction poll() {
    return queue.poll();
  }

  @SuppressWarnings("UnusedDeclaration")
  public void add(IAction action) {
    queue.offer(action);
    //noinspection CallToNotifyInsteadOfNotifyAll,NotifyNotInSynchronizedContext
  }

  public IAction peek() {
    return queue.peek();
  }

}
