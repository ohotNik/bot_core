package ru.ohotnik.bot.core.action;

import javax.annotation.Nonnull;

/**
 * Created by ohotNik
 * Date : 25.10.14
 * Time : 23:33
 * Description :
 */
@SuppressWarnings({"UnusedDeclaration",
    "AbstractClassNeverImplemented", "ComparableImplementedButEqualsNotOverridden"})
public abstract class AbstractAction implements IAction {

  private Long time;

  @Override
  public long getTime() {
    return time != null ? time : 0L;
  }

  public void setTime(Long time) {
    this.time = time;
  }

  @SuppressWarnings("MethodParameterNamingConvention")
  @Override
  public int compareTo(@Nonnull Object o) {
    if (o instanceof AbstractAction) {
      return Long.valueOf(getTime()).compareTo(getTime());
    }
    return 0;
  }
}
