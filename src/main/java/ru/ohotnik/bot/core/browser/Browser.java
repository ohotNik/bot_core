package ru.ohotnik.bot.core.browser;

import org.openqa.selenium.WebDriver;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ohotNik
 * Date : 18.10.14
 * Time : 13:36
 * Description :
 */
public class Browser {

  @SuppressWarnings({"RedundantFieldInitialization", "StaticNonFinalField"})
  private static WebDriver s_webDriver = null;

  private Browser() {
  }

  @SuppressWarnings({"CallToPrintStackTrace", "SynchronizedMethod", "UnusedDeclaration"})
  public static synchronized WebDriver getInstance(Type type) {
    //noinspection StaticVariableUsedBeforeInitialization
    if (s_webDriver == null) {
      Class<? extends WebDriver> clazz = type.getClassValue();
      try {
        s_webDriver = clazz.getConstructor().newInstance();
      } catch (InstantiationException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
    return s_webDriver;
  }

  @SuppressWarnings({"SynchronizedMethod", "UnusedDeclaration"})
  public static synchronized WebDriver getInstance() {
    assert s_webDriver != null;
    return s_webDriver;
  }

}
