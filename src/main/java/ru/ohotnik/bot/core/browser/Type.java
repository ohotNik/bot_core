package ru.ohotnik.bot.core.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Created by ohotNik
 * Date : 18.10.14
 * Time : 13:39
 * Description :
 */
@SuppressWarnings("UnusedDeclaration")
public enum Type {
  FIREFOX(FirefoxDriver.class),
  HTMLUNIT(HtmlUnitDriver.class),
  CHROME(ChromeDriver.class),
  IE(InternetExplorerDriver.class);

  private Class<? extends WebDriver> clazz;

  Type(Class<? extends WebDriver> clazz) {
    this.clazz = clazz;
  }

  public Class<? extends WebDriver> getClassValue() {
    return clazz;
  }

}
